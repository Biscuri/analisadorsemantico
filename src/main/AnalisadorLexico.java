package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import model.Erro;
import model.Padrao;
import model.Reservadas;
import model.Token;
import exceptions.ArquivoNaoEncontradoException;
import exceptions.TokenNaoConhecidoException;

import java.io.FileWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnalisadorLexico {
	private ArrayList<Token> tokens;
	private ArrayList<Erro> erros;
	private String auxString = "";
	private boolean escrevendoString = false;
	private boolean tokenizaString = false;
	private boolean escrevendoCaractere = false;
	private boolean tokenizaCaractere = false;
	private boolean escrevendoComentario = false;
	private int numLinha = 0;

	public AnalisadorLexico() {
		tokens = new ArrayList<Token>();
		erros = new ArrayList<Erro>();
	}

	public ArrayList<Token> Analisa() throws ArquivoNaoEncontradoException, IOException {
		ArrayList<Token> retorno = new ArrayList<Token>();
		
		File dir = new File(System.getProperty("user.dir"));
		File listaDeArquivos[] = dir.listFiles();
		for (File f : listaDeArquivos) {
			if (!f.exists()) {
				throw new ArquivoNaoEncontradoException();
			} else {
				String nameFile = f.getName();
				if (nameFile.endsWith("txt")) {
					if (!nameFile.startsWith("s_")) {
						FileReader fr = new FileReader(f);
						BufferedReader br = new BufferedReader(fr);
						while (br.ready()) {
							numLinha++;
							String linha = br.readLine();
							if (escrevendoString) { // Se esta escrevendo
								// uma string e pulou
								// uma linha, e um
								// erro de string mal
								// formada.
								erros.add(new Erro(auxString, 2, numLinha));
								escrevendoString = false;
							}
							if (escrevendoComentario) {
								if (linha.contains("}")) {
									linha = linha.replaceFirst(".*\\}", "");
									System.out.println(linha);
									escrevendoComentario = false;
								}
							}

							linha = linha.replaceAll("\\{[^\\}]*\\}", "");

							if (linha.contains("{")) {
								linha = linha.replaceFirst("\\{.*", "");
								analisaLinha(linha);
								escrevendoComentario = true;
							}

							if (!escrevendoComentario) {
								analisaLinha(linha); // Analisa a proxima
								// linha para separar os
								// tokens.
							}
						}

						// Se terminou de ler e nao fechou string, caractere
						// ou comentario, ha uma ma formacao.
						if (escrevendoString) {
							erros.add(new Erro(auxString, 2, numLinha));
						}
						if (escrevendoCaractere) {
							erros.add(new Erro(auxString, 3, numLinha));
						}
						if (escrevendoComentario) {
							erros.add(new Erro("", 4, numLinha));
						}
						br.close();
						fr.close();

						//							imprimeSaida();
						//							geraTxt(nameFile);
						retorno = tokens;
						resetaAtributos();

					}
				}
			}
		}
		return retorno;
	}

	private void analisaLinha(String linha) {

		String[] palavras = linha.split("\\s+"); // Separa as palavras por
													// espacos.
		for (String p : palavras) {

			if (p.trim().isEmpty())
				continue;

			char[] palavra = p.toCharArray(); // Transforma a palavra num array
												// para ser analisada.
			// Verifica se e um comeco ou fim de
			// String e seta os booleanos de
			// escrever string ou fechar string.

			if (p.contains("\"") && p.contains("\'")) {
				int aspadupla = p.indexOf("\"");
				int aspasimples = p.indexOf("'");
				if (aspadupla < aspasimples)
					if (!escrevendoCaractere)
						trataString(palavra);
					else if (!escrevendoString)
						trataCaractere(palavra);
			} else {
				if (!escrevendoCaractere && p.contains("\"")) {
					// tratamento especial para strings
					trataString(palavra);
					continue;
				}

				if (!escrevendoString && p.contains("\'")) {
					// tratamento especial para caractere
					trataCaractere(palavra);
					continue;
				}
			}

			if (escrevendoString) { // Se estou escrevendo uma string, nao
				auxString += " " + p; // faco tokens ainda.
			} else if (escrevendoCaractere) { // Mesma coisa pra caractere
				auxString += " " + p;
			} else { // Tenta criar um token com todos os caracteres, se nao
				// conseguir, reduz ate
				// encontrar um token valido, ou definir como
				// s�mbolo invalido.
				try {
					Token t = new Token(p, numLinha);
					tokens.add(t);
				} catch (TokenNaoConhecidoException e) {
					verificaLexema(palavra);
				}
			}
		}
	}

	private void trataString(char[] palavra) {
		String aux = "";
		if (escrevendoString) {
			for (int i = 0; i < palavra.length; i++) {
				aux += palavra[i];
				if (Pattern.matches(".*\"", aux)) {
					auxString += " " + aux;
					escrevendoString = false;
					tokenizaString = true;
					verificaToken(auxString);
					aux = "";
				}
			}
			if (!aux.trim().isEmpty()) {
				palavra = aux.toCharArray();
				verificaLexema(palavra);
			}
		} else {
			for (int i = 0; i < palavra.length; i++) {
				String auxAnterior = aux;
				aux += palavra[i];
				try {
					if (Pattern.matches("\\d+\\.", aux))
						continue;

					if (Pattern.matches("\".*\"", aux)) {
						tokenizaString = true;
						verificaToken(aux);
						aux = "";
						continue;
					}
					if (Pattern.matches("\".*", aux))
						continue;
					new Token(aux, numLinha);
				} catch (TokenNaoConhecidoException ex) {
					if (!auxAnterior.trim().isEmpty()) {
						verificaToken(auxAnterior);
						aux = "" + palavra[i];
					}
				}
			}
			if (aux.contains("\"")) {
				escrevendoString = true;
				auxString = "";
				auxString += aux;
			} else {
				verificaToken(aux);
			}
		}
	}

	private void trataCaractere(char[] palavra) {
		String aux = "";
		if (escrevendoCaractere) {
			for (int i = 0; i < palavra.length; i++) {
				aux += palavra[i];
				if (Pattern.matches(".*'", aux)) {
					auxString += " " + aux;
					escrevendoCaractere = false;
					tokenizaCaractere = true;
					verificaToken(auxString);
					aux = "";
				}
			}
			if (!aux.trim().isEmpty()) {
				palavra = aux.toCharArray();
				verificaLexema(palavra);
			}
		} else {
			for (int i = 0; i < palavra.length; i++) {
				String auxAnterior = aux;
				aux += palavra[i];
				try {
					if (Pattern.matches("\\d+\\.", aux))
						continue;

					if (Pattern.matches("'.*'", aux)) {
						tokenizaCaractere = true;
						verificaToken(aux);
						aux = "";
						continue;
					}
					if (Pattern.matches("'.*", aux))
						continue;
					new Token(aux, numLinha);
				} catch (TokenNaoConhecidoException ex) {
					if (!auxAnterior.trim().isEmpty()) {
						verificaToken(auxAnterior);
						aux = "" + palavra[i];
					}
				}
			}
			if (!aux.trim().isEmpty()) {
				if (aux.contains("'")) {
					escrevendoCaractere = true;
					auxString = "";
					auxString += aux;
				} else {
					verificaToken(aux);
				}
			}
		}
	}

	/*
	 * Percorre a palavra char por char e separa tokens nao espacados. Tenta
	 * fazer um token com a maior string, se for invalida, tenta fazer com a
	 * maior string valida. Ex.: "i=10". Tenta criar um token com "i=", nao
	 * consegue, e faz um token com "i", depois tenta com "=1", nao consegue
	 * cria com "=", depois cria com "10", separando os tokens nao espacados. Se
	 * for um unico token, cria o token ao sair do for.
	 */

	private void verificaLexema(char[] palavra) {
		String aux = "";
		for (int i = 0; i < palavra.length; i++) {
			String auxAnterior = aux;
			aux += palavra[i];
			try {
				if (Pattern.matches("\\d+\\.", aux))
					continue;
				new Token(aux, numLinha);
			} catch (TokenNaoConhecidoException ex) {
				if (!auxAnterior.trim().isEmpty()) {
					verificaToken(auxAnterior);
					aux = "" + aux.charAt(aux.length() - 1);
				}
			}
		}
		verificaToken(aux);
	}

	private void verificaToken(String p) {
		Token aux;
		try {
			aux = new Token(p, numLinha);
			tokens.add(aux);
		} catch (TokenNaoConhecidoException e) {
			if (tokenizaString){
				tokenizaString = false;
				erros.add(new Erro(p, 2, numLinha));
			}
			else if (tokenizaCaractere){
				tokenizaCaractere = false;
				erros.add(new Erro(p, 3, numLinha));
			}
			else
				erros.add(new Erro(p, 1, numLinha));
		}
	}

	private void imprimeSaida() {

		for (Token t : tokens) {
			System.out.println(t.toString());
		}
		if (erros.isEmpty()) {
			System.out.println("Sucess!!!");
		} else {
			System.out.println("\n");
			for (Erro e : erros) {
				System.out.println(e.toString());
			}
		}
	}

	private void geraTxt(String nameFile) {

		String[] nome = new String[2];
		nome = nameFile.split("\\.");
		nameFile = nome[0];
		//System.out.println(nameFile);
		File file = new File("s_" + nameFile + ".txt");
		try {
			FileWriter f = new FileWriter(file, false);

			for (Token t : tokens) {
				f.write(t.toString() + "\r\n");
			}
			if (erros.isEmpty()) {
				f.write("Sucess!!!");
			} else {
				f.write("\r\n");
				for (Erro e : erros) {
					f.write(e.toString() + "\r\n");
				}
			}
			f.close();

			tokens.clear();
			erros.clear();

		} catch (IOException ex) {
			Logger.getLogger(AnalisadorLexico.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}

	public void resetaAtributos() {
		tokens = new ArrayList<Token>();
		erros = new ArrayList<Erro>();
		auxString = "";
		escrevendoString = false;
		tokenizaString = false;
		escrevendoCaractere = false;
		tokenizaCaractere = false;
		escrevendoComentario = false;
		numLinha = 0;
	}
}
