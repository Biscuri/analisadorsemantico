package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

import model.Constante;
import model.Erro;
import model.Funcao;
import model.Padrao;
import model.Terminal;
import model.Tipo;
import model.Token;
import model.Variavel;
import model.VariavelProvisoria;
import exceptions.ArquivoNaoEncontradoException;

public class AnalisadorSemantico {

	private static boolean temErro = false; // atributo para dizer se existem ou
											// nao erros
	private static ArrayList<Erro> erros;
	private static ArrayList<Token> tokens; // Lista de tokens
	private static AnalisadorLexico lexico;
	private static Hashtable<String, Constante> constantes;
	private static Hashtable<String, Variavel> variaveis;
	private static Hashtable<String, Funcao> funcoes;
	private static Hashtable<String, Variavel> parametros;
	private static ArrayList<Variavel> parametros2;
	private static String identificador = "";
	private static ArrayList<Token> valor;
	private static Terminal tipo = null;
	private static int id = 0;
	private static int auxid = 0;
	private static int dimensoes = 0;

	public static void main(String[] args)
			throws ArquivoNaoEncontradoException, IOException {
		lexico = new AnalisadorLexico();
		erros = new ArrayList<Erro>();
		tokens = new ArrayList<Token>();
		constantes = new Hashtable<String, Constante>();
		variaveis = new Hashtable<String, Variavel>();
		funcoes = new Hashtable<String, Funcao>();
		parametros = new Hashtable<String, Variavel>();
		parametros2 = new ArrayList<Variavel>();
		valor = new ArrayList<Token>();
		criarTabelaSimbolos();
	}

	public static void criarTabelaSimbolos() {
		try {
			tokens = lexico.Analisa();
			System.out.println(5 > 2 && 8 + 2 > 4);
			while (id < tokens.size()) {
				// caso seja um bloco de constantes
				if (tokens.get(id).terminal == Terminal.CONST) {

					System.out.println("Inicio do bloco de constantes \n");
					// pula o "inicio"
					id += 2;
					// enquanto nao chegar ao fim do bloco
					while (!(tokens.get(id).terminal == Terminal.FIM)) {
						// salvando o tipo da constante
						tipo = tokens.get(id).terminal;
						// criando nova constante na tabela
						pegaDadosConst(tipo);
						// caso haja mais constantes do mesmo tipo
						if (tokens.get(id).terminal == Terminal.VIRGULA) {
							while (!(tokens.get(id).terminal == Terminal.PONTOEVIRGULA)) {
								pegaDadosConst(tipo);
							}
							// caso as proximas constantes sejam de outro tipo
						}
						if (tokens.get(id).terminal == Terminal.PONTOEVIRGULA) {
							tipo = null;
							id++;
						}
					}
					// caso seja um bloco de variaveis
				}
				if (tokens.get(id).terminal == Terminal.VAR) {

					System.out.println("\n \nInicio do bloco de variaveis \n");
					// pula o "inicio"
					id += 2;
					// enquanto nao chegar ao fim do bloco
					while (!(tokens.get(id).terminal == Terminal.FIM)) {
						// salvando o tipo da variavel
						tipo = tokens.get(id).terminal;
						// criando nova variavel na tabela
						pegaDadosVar(tipo, variaveis);
						// caso haja mais variaveis do mesmo tipo
						if (tokens.get(id).terminal == Terminal.VIRGULA) {
							while (!(tokens.get(id).terminal == Terminal.PONTOEVIRGULA)) {
								pegaDadosVar(tipo, variaveis);
							}
							// caso as proximas variaveis sejam de outro tipo
						}
						if (tokens.get(id).terminal == Terminal.PONTOEVIRGULA) {
							tipo = null;
							id++;
						}
					}
					// caso seja a funcao programa
				}
				if (tokens.get(id).terminal == Terminal.PROGRAMA) {

					System.out.println("\n \nInicio da main \n");

					id++;
					if (tokens.get(id).terminal == Terminal.INICIO) {
						// criando nova funcao na tabela
						CriaMain();
					}
					// caso seja outra funcao qualquer
				}
				if (tokens.get(id).terminal == Terminal.FUNCAO) {

					System.out.println("\n \nInicio do bloco de funcao \n");

					id++;
					// caso tenha retorno
					if (tokens.get(id).terminal == Terminal.INTEIRO
							|| tokens.get(id).terminal == Terminal.REAL
							|| tokens.get(id).terminal == Terminal.BOOLEANO
							|| tokens.get(id).terminal == Terminal.CADEIA
							|| tokens.get(id).terminal == Terminal.CARACTERE) {
						// salvando o tipo da funcao
						tipo = tokens.get(id).terminal;
						id++;
					}
					// criando nova funcao na tabela
					String funcaoAtual = pegaDadosFunc(tipo);
					id++;

					// analisando o corpo da função
					analisaCorpo(funcaoAtual);

				}
				id++;
			}

		} catch (ArquivoNaoEncontradoException | IOException e) {
			e.printStackTrace();
		}

	}

	public static void pegaDadosConst(Terminal tipo) {

		id++;
		// salvando o identificador
		identificador = tokens.get(id).conteudo;
		// pulando o "="
		id += 2;

		valor.clear();

		while (true) {
			// se achar uma virgula ou ponto e virgula eh pq terminou a
			// constante
			if (tokens.get(id).terminal == Terminal.VIRGULA
					|| tokens.get(id).terminal == Terminal.PONTOEVIRGULA) {
				break;
				// enquanto nao achar, tudo eh valor, pois pode ser uma
				// expressao
			} else {
				valor.add(tokens.get(id));
				id++;
			}
		}

		// Verifica se o tipo do valor est� correto
		auxid = 0;
		Terminal tipoConteudo = verificarTipoConst(valor);
		if (tipoConteudo != tipo) {
			if (!(tipo == Terminal.REAL && tipoConteudo == Terminal.INTEIRO)) {
				Erro e = new Erro(identificador, 0, tokens.get(id).getLinha());
				erros.add(e);
				System.out.println(e.toString());
			}
		}

		// criacao de fato da constante
		Constante constante = new Constante(valor, tipo, tokens.get(id)
				.getLinha());
		// adicionando a constante na tabela
		constantes.put(identificador, constante);
		System.out.println(identificador);
		System.out.println(constante.toString());
		valor.clear();

	}

	public static void pegaDadosVar(Terminal tipo,
			Hashtable<String, Variavel> escopo) {

		id++;

		// caso seja matriz
		if (tokens.get(id).terminal == Terminal.OP_MENOR) {
			// pula o proximo simbolo de menor
			id += 2;

			while (!(tokens.get(id).terminal == Terminal.OP_MAIOR)) {

				// pulando a virgula
				if (!(tokens.get(id).terminal == Terminal.VIRGULA)) {
					dimensoes++;
				}

				id++;

			}

			// pula o proximo simbolo de maior
			id += 2;

		}

		// salvando o identificador
		identificador = tokens.get(id).conteudo;

		id++;

		// criacao de fato da variavel
		Variavel variavel = new Variavel(valor, tipo, dimensoes);
		dimensoes = 0;
		// adicionando a parametro na variavel
		escopo.put(identificador, variavel);
		System.out.println(variavel.getTipo());
		System.out.println(variavel.getDimensoes());
		System.out.println(identificador);

	}

	private static Terminal verificarTipoVar(ArrayList<Token> tokens,
			Hashtable<String, Variavel> escopo) {
		Terminal tipo = null;

		Token t1 = tokens.get(auxid);

		switch (t1.terminal) {
		case NUMERO:
			if (t1.conteudo.contains(".")) {
				tipo = Terminal.REAL;
			} else {
				tipo = Terminal.INTEIRO;
			}
			break;
		case STRING:
			tipo = Terminal.CADEIA;
			break;
		case CHAR:
			tipo = Terminal.CARACTERE;
			break;
		case VERDADEIRO:
			tipo = Terminal.BOOLEANO;
			break;
		case FALSO:
			tipo = Terminal.BOOLEANO;
			break;
		case IDENTIFICADOR:
			Variavel v = escopo.get(t1.conteudo);
			if (v != null) {
				tipo = v.getTipo();
			} else {
				Constante c = constantes.get(t1.conteudo);
				if (c != null) {
					tipo = c.getTipo();
				} else {
					if (tipo == null) {
						v = variaveis.get(t1.conteudo);
						if (v != null)
							tipo = v.getTipo();
					}
				}
			}
			break;
		case ABREPARENTESE:
			auxid++;
			tipo = verificarTipoVar(tokens, escopo);
		case FECHAPARENTESE:
			return tipo;
		case OP_MENOR:
			while (true) {
				if (t1.terminal == Terminal.OP_MAIOR) {
					auxid += 2;
					Token aux = tokens.get(auxid);
					Variavel va = escopo.get(aux.conteudo);
					if (va != null) {
						tipo = va.getTipo();
					} else {
						Constante co = constantes.get(aux.conteudo);
						if (co != null) {
							tipo = co.getTipo();
						} else {
							if (tipo == null) {
								va = variaveis.get(aux.conteudo);
								if (va != null)
									tipo = va.getTipo();
							}
						}
					}
					break;
				}
			}
			auxid++;
			break;
		case OP_SOMA:
			tipo = Terminal.INTEIRO;
			auxid++;
			Token temp = tokens.get(auxid);
			if (temp.terminal == Terminal.NUMERO) {
				if (temp.conteudo.contains(".")) {
					tipo = Terminal.REAL;
				} else {
					tipo = Terminal.INTEIRO;
				}
			} else {
				Erro e = new Erro(identificador, 0, temp.linha);
				erros.add(e);
				System.out.println(e.toString());
			}
			break;
		case OP_SUB:
			tipo = Terminal.INTEIRO;
			auxid++;
			Token temp2 = tokens.get(auxid);
			if (temp2.terminal == Terminal.NUMERO) {
				if (temp2.conteudo.contains(".")) {
					tipo = Terminal.REAL;
				} else {
					tipo = Terminal.INTEIRO;
				}
			} else {
				Erro e = new Erro(identificador, 0, temp2.linha);
				erros.add(e);
				System.out.println(e.toString());
			}
			break;
		case OP_NAO:
			tipo = Terminal.BOOLEANO;
			auxid++;
			Terminal tipoDir = verificarTipoVar(tokens, escopo);
			if (tipoDir != Terminal.BOOLEANO) {
				Erro e = new Erro(identificador, 0, t1.linha);
				erros.add(e);
				System.out.println(e.toString());
			}
			break;
		}

		auxid++;

		while (auxid < tokens.size()) {

			Token op = tokens.get(auxid);
			Token t2 = null;
			if (auxid < tokens.size() - 1)
				t2 = tokens.get(++auxid);

			Terminal tipo2 = null;

			if (t2 != null) {
				switch (t2.terminal) {
				case NUMERO:
					if (t2.conteudo.contains(".")) {
						tipo2 = Terminal.REAL;
					} else {
						tipo2 = Terminal.INTEIRO;
					}
					break;
				case STRING:
					tipo2 = Terminal.CADEIA;
					break;
				case CHAR:
					tipo2 = Terminal.CARACTERE;
					break;
				case VERDADEIRO:
					tipo2 = Terminal.BOOLEANO;
					break;
				case FALSO:
					tipo2 = Terminal.BOOLEANO;
					break;
				case IDENTIFICADOR:
					Variavel v = escopo.get(t1.conteudo);
					if (v != null) {
						tipo = v.getTipo();
					} else {
						Constante c = constantes.get(t1.conteudo);
						if (c != null) {
							tipo = c.getTipo();
						} else {
							if (tipo == null) {
								v = variaveis.get(t1.conteudo);
								if (v != null)
									tipo = v.getTipo();
							}
						}
					}
					break;
				case ABREPARENTESE:
					tipo2 = verificarTipoVar(tokens, escopo);
				case FECHAPARENTESE:
					return tipo;
				case OP_MENOR:
					while (true) {
						if (t1.terminal == Terminal.OP_MAIOR) {
							auxid += 2;
							Token aux = tokens.get(auxid);
							Variavel va = escopo.get(aux.conteudo);
							if (va != null) {
								tipo = va.getTipo();
							} else {
								Constante co = constantes.get(aux.conteudo);
								if (co != null) {
									tipo = co.getTipo();
								} else {
									if (tipo == null) {
										va = variaveis.get(aux.conteudo);
										if (va != null)
											tipo = va.getTipo();
									}
								}
							}
							break;
						}
					}
					auxid++;
					break;
				case OP_SOMA:
					tipo = Terminal.INTEIRO;
					auxid++;
					Token temp = tokens.get(auxid);
					if (temp.terminal != Terminal.INTEIRO
							&& temp.terminal != Terminal.REAL) {
						Erro e = new Erro(identificador, 0, temp.linha);
						erros.add(e);
						System.out.println(e.toString());
					}
					break;
				case OP_SUB:
					tipo = Terminal.INTEIRO;
					auxid++;
					Token temp2 = tokens.get(auxid);
					if (temp2.terminal != Terminal.INTEIRO
							&& temp2.terminal != Terminal.REAL) {
						Erro e = new Erro(identificador, 0, temp2.linha);
						erros.add(e);
						System.out.println(e.toString());
					}
					break;
				case OP_NAO:
					tipo = Terminal.BOOLEANO;
					auxid++;
					Terminal tipoDir = verificarTipoVar(tokens, escopo);
					if (tipoDir != Terminal.BOOLEANO) {
						Erro e = new Erro(identificador, 0, t2.linha);
						erros.add(e);
						System.out.println(e.toString());
					}
					break;
				}
			}

			switch (op.padrao) {
			case OPARITMETICO:
				if (tipo == Terminal.INTEIRO) {
					tipo = tipo2;
				} else if (tipo2 == Terminal.INTEIRO) {
					tipo = tipo;
				} else if (tipo == Terminal.REAL && tipo2 == Terminal.REAL) {
					tipo = tipo;
				} else {
					Erro e = new Erro(identificador, 0, t2.linha);
					erros.add(e);
					System.out.println(e.toString());
					tipo = Terminal.INTEIRO;
				}
				break;
			case OPRELACIONAL:
				tipo2 = verificarTipoVar(tokens, escopo);
				if ((tipo != Terminal.INTEIRO && tipo != Terminal.REAL)
						|| (tipo2 != Terminal.INTEIRO && tipo2 != Terminal.REAL)) {
					Erro e = new Erro(identificador, 0, t2.linha);
					erros.add(e);
					System.out.println(e.toString());
				}
				tipo = Terminal.BOOLEANO;
				break;
			case OPLOGICO:
				tipo2 = verificarTipoVar(tokens, escopo);
				if (tipo != Terminal.BOOLEANO || tipo2 != Terminal.BOOLEANO) {
					Erro e = new Erro(identificador, 0, t2.linha);
					erros.add(e);
					System.out.println(e.toString());
				}
				tipo = Terminal.BOOLEANO;
				break;
			case FECHAPARENTESE:
				return tipo;
			}

			auxid++;
		}

		return tipo;
	}

	private static Terminal verificarTipoConst(ArrayList<Token> tokens) {
		Terminal tipo = null;

		Token t1 = tokens.get(auxid);

		switch (t1.terminal) {
		case NUMERO:
			if (t1.conteudo.contains(".")) {
				tipo = Terminal.REAL;
			} else {
				tipo = Terminal.INTEIRO;
			}
			break;
		case STRING:
			tipo = Terminal.CADEIA;
			break;
		case CHAR:
			tipo = Terminal.CARACTERE;
			break;
		case VERDADEIRO:
			tipo = Terminal.BOOLEANO;
			break;
		case FALSO:
			tipo = Terminal.BOOLEANO;
			break;
		case IDENTIFICADOR:
			Constante c = constantes.get(t1.conteudo);
			if (c != null) {
				tipo = c.getTipo();
			} else {
				if (tipo == null)
					tipo = Terminal.INTEIRO;
				Erro e = new Erro(identificador, 1, t1.linha);
				erros.add(e);
				System.out.println(e.toString());
			}
			break;
		case ABREPARENTESE:
			auxid++;
			tipo = verificarTipoConst(tokens);
		case FECHAPARENTESE:
			return tipo;
		case OP_SOMA:
			tipo = Terminal.INTEIRO;
			auxid++;
			Token temp = tokens.get(auxid);
			if (temp.terminal == Terminal.NUMERO) {
				if (temp.conteudo.contains(".")) {
					tipo = Terminal.REAL;
				} else {
					tipo = Terminal.INTEIRO;
				}
			} else {
				Erro e = new Erro(identificador, 0, temp.linha);
				erros.add(e);
				System.out.println(e.toString());
			}
			break;
		case OP_SUB:
			tipo = Terminal.INTEIRO;
			auxid++;
			Token temp2 = tokens.get(auxid);
			if (temp2.terminal == Terminal.NUMERO) {
				if (temp2.conteudo.contains(".")) {
					tipo = Terminal.REAL;
				} else {
					tipo = Terminal.INTEIRO;
				}
			} else {
				Erro e = new Erro(identificador, 0, temp2.linha);
				erros.add(e);
				System.out.println(e.toString());
			}
			break;
		case OP_NAO:
			tipo = Terminal.BOOLEANO;
			auxid++;
			Terminal tipoDir = verificarTipoConst(tokens);
			if (tipoDir != Terminal.BOOLEANO) {
				Erro e = new Erro(identificador, 0, t1.linha);
				erros.add(e);
				System.out.println(e.toString());
			}
			break;
		}

		auxid++;

		while (auxid < tokens.size()) {

			Token op = tokens.get(auxid);
			Token t2 = null;
			if (auxid < tokens.size() - 1)
				t2 = tokens.get(++auxid);

			Terminal tipo2 = null;

			if (t2 != null) {
				switch (t2.terminal) {
				case NUMERO:
					if (t2.conteudo.contains(".")) {
						tipo2 = Terminal.REAL;
					} else {
						tipo2 = Terminal.INTEIRO;
					}
					break;
				case STRING:
					tipo2 = Terminal.CADEIA;
					break;
				case CHAR:
					tipo2 = Terminal.CARACTERE;
					break;
				case VERDADEIRO:
					tipo2 = Terminal.BOOLEANO;
					break;
				case FALSO:
					tipo2 = Terminal.BOOLEANO;
					break;
				case IDENTIFICADOR:
					Constante c = constantes.get(t1.conteudo);
					if (c != null) {
						tipo = c.getTipo();
					} else {
						if (tipo == null)
							tipo = Terminal.INTEIRO;
						Erro e = new Erro(identificador, 1, t1.linha);
						erros.add(e);
						System.out.println(e.toString());
					}
					break;
				case ABREPARENTESE:
					tipo2 = verificarTipoConst(tokens);
				case FECHAPARENTESE:
					return tipo;
				case OP_SOMA:
					tipo = Terminal.INTEIRO;
					auxid++;
					Token temp = tokens.get(auxid);
					if (temp.terminal != Terminal.INTEIRO
							&& temp.terminal != Terminal.REAL) {
						Erro e = new Erro(identificador, 0, temp.linha);
						erros.add(e);
						System.out.println(e.toString());
					}
					break;
				case OP_SUB:
					tipo = Terminal.INTEIRO;
					auxid++;
					Token temp2 = tokens.get(auxid);
					if (temp2.terminal != Terminal.INTEIRO
							&& temp2.terminal != Terminal.REAL) {
						Erro e = new Erro(identificador, 0, temp2.linha);
						erros.add(e);
						System.out.println(e.toString());
					}
					break;
				case OP_NAO:
					tipo = Terminal.BOOLEANO;
					auxid++;
					Terminal tipoDir = verificarTipoConst(tokens);
					if (tipoDir != Terminal.BOOLEANO) {
						Erro e = new Erro(identificador, 0, t2.linha);
						erros.add(e);
						System.out.println(e.toString());
					}
					break;
				}
			}

			switch (op.padrao) {
			case OPARITMETICO:
				if (tipo == Terminal.INTEIRO) {
					tipo = tipo2;
				} else if (tipo2 == Terminal.INTEIRO) {
					tipo = tipo;
				} else if (tipo == Terminal.REAL && tipo2 == Terminal.REAL) {
					tipo = tipo;
				} else {
					Erro e = new Erro(identificador, 0, t2.linha);
					erros.add(e);
					System.out.println(e.toString());
					tipo = Terminal.INTEIRO;
				}
				break;
			case OPRELACIONAL:
				tipo2 = verificarTipoConst(tokens);
				if ((tipo != Terminal.INTEIRO && tipo != Terminal.REAL)
						|| (tipo2 != Terminal.INTEIRO && tipo2 != Terminal.REAL)) {
					Erro e = new Erro(identificador, 0, t2.linha);
					erros.add(e);
					System.out.println(e.toString());
				}
				tipo = Terminal.BOOLEANO;
				break;
			case OPLOGICO:
				tipo2 = verificarTipoConst(tokens);
				if (tipo != Terminal.BOOLEANO || tipo2 != Terminal.BOOLEANO) {
					Erro e = new Erro(identificador, 0, t2.linha);
					erros.add(e);
					System.out.println(e.toString());
				}
				tipo = Terminal.BOOLEANO;
				break;
			case FECHAPARENTESE:
				return tipo;
			}

			auxid++;
		}

		return tipo;
	}

	public static void CriaMain() {
		// criacao de fato da funcao
		Funcao programa = new Funcao();
		// adicionando a funcao na tabela
		funcoes.put("programa", programa);
		System.out.println(programa.getTipo());
		System.out.println(programa.getValor());
		System.out.println(programa.getParametros());

	}

	public static String pegaDadosFunc(Terminal tipo) {

		// salvando o identificador
		identificador = tokens.get(id).conteudo;

		VariavelProvisoria param;
		Variavel parametro;
		// pulando o "("
		id += 2;
		System.out.println("\nParametros da funcao \n");
		// enquanto nao achar o ")"
		while (!(tokens.get(id).terminal == Terminal.FECHAPARENTESE)) {
			// criando novo parametro
			param = pegaDadosParam();
			parametro = new Variavel(param.getValor(), param.getTipo(),
					param.getDimensoes());
			parametros.clear();
			parametros2.clear();
			// adicionando o parametro na tabela
			parametros.put(param.getIdentificador(), parametro);
			parametros2.add(parametro);
			// caso haja mais Parametros
			if (tokens.get(id).terminal == Terminal.VIRGULA) {
				while (!(tokens.get(id).terminal == Terminal.FECHAPARENTESE)) {
					id++;
					// criando novo parametro
					param = pegaDadosParam();
					parametro = new Variavel(param.getValor(), param.getTipo(),
							param.getDimensoes());
					// adicionando o parametro na tabela
					parametros.put(param.getIdentificador(), parametro);
				}
			}
		}
		id++;
		if (tokens.get(id).terminal == Terminal.INICIO) {
			Funcao funcao;
			System.out.println("\nFuncao \n");
			// criacao de fato da funcao
			if (tipo != null) {
				funcao = new Funcao(valor, tipo);
				System.out.println(identificador);
				System.out.println(funcao.getTipo());
				System.out.println(funcao.getValor());
				System.out.println(funcao.getParametros());
			} else {
				funcao = new Funcao();
				System.out.println("apenas vendo se ta criando certo /ignore");
				System.out.println(identificador);
				System.out.println(funcao.getParametros());
			}

			funcao.getParametros().addAll(parametros2);
			funcao.getVariaveis().putAll(parametros);
			funcoes.put(identificador, funcao);

		}

		return identificador;

	}

	public static VariavelProvisoria pegaDadosParam() {

		Terminal tipoParametro = tokens.get(id).terminal;
		id++;
		// caso seja matriz
		if (tokens.get(id).terminal == Terminal.OP_MENOR) {
			// pula o proximo simbolo de menor
			id += 2;

			while (!(tokens.get(id).terminal == Terminal.OP_MAIOR)) {

				// pulando a virgula
				if (!(tokens.get(id).terminal == Terminal.VIRGULA)) {
					dimensoes++;
				}

				id++;

			}

			// pula o proximo simbolo de maior
			id += 2;

		}

		// salvando o identificador
		String identificadorParam = tokens.get(id).conteudo;

		id++;

		// criacao de fato do parametro
		VariavelProvisoria parametro = new VariavelProvisoria(valor,
				tipoParametro, dimensoes, identificadorParam);
		dimensoes = 0;

		System.out.println(parametro.getTipo());
		System.out.println(parametro.getDimensoes());
		System.out.println(parametro.getIdentificador());

		return parametro;

	}

	public static void analisaCorpo(String funcao) {

		// procurando a funcao atual na tabela de funcoes
		Funcao funcAtual = funcoes.get(funcao);

		// Caso haja um bloco de variaveis locais;
		if (tokens.get(id).terminal == Terminal.VAR) {

			System.out.println("\n \nInicio do bloco de variaveis \n");
			// pula o "inicio"
			id += 2;
			// enquanto nao chegar ao fim do bloco
			while (!(tokens.get(id).terminal == Terminal.FIM)) {
				// salvando o tipo da variavel
				tipo = tokens.get(id).terminal;
				// criando nova variavel na tabela
				pegaDadosVar(tipo, funcAtual.getVariaveis());
				// caso haja mais variaveis do mesmo tipo
				if (tokens.get(id).terminal == Terminal.VIRGULA) {
					while (!(tokens.get(id).terminal == Terminal.PONTOEVIRGULA)) {
						pegaDadosVar(tipo, funcAtual.getVariaveis());
					}
					// caso as proximas variaveis sejam de outro tipo
				}
				if (tokens.get(id).terminal == Terminal.PONTOEVIRGULA) {
					tipo = null;
					id++;
				}
			}
			// Caso seja chamada de função ou atribuição
		}
		if (tokens.get(id).terminal == Terminal.IDENTIFICADOR) {

			identificador = tokens.get(id).conteudo;
			id++;
			// caso seja chamada de função
			if (tokens.get(id).terminal == Terminal.ABREPARENTESE) {

				verificaChamada(identificador);
				id++;
				// caso seja atribuição
			}
			if (tokens.get(id).terminal == Terminal.OP_IGUAL) {

				valor.clear();

				while (true) {
					// se achar um ponto e virgula eh pq terminou a atribuicao
					if (tokens.get(id).terminal == Terminal.PONTOEVIRGULA) {
						break;
						// enquanto nao achar, tudo eh valor, pois pode ser uma
						// expressao
					} else {
						valor.add(tokens.get(id));
						id++;
					}
				}

				// Verifica se o tipo do valor esta correto
				auxid = 0;
				Terminal tipoConteudo = verificarTipoVar(valor, funcAtual.getVariaveis());
				Terminal tipoVar = variaveis.get(identificador).getTipo();
				if (tipoConteudo != tipoVar) {
					if (!(tipoVar == Terminal.REAL && tipoConteudo == Terminal.INTEIRO)) {
						Erro e = new Erro(identificador, 0, tokens.get(id)
								.getLinha());
						erros.add(e);
						System.out.println(e.toString());
					}
				}

			}
			// caso seja retorno de função
		}
		if (tokens.get(id).terminal == Terminal.OP_IGUAL) {

			// pulando o "=" e o ">"
			id += 2;
			// caso seja matriz
			if (tokens.get(id).terminal == Terminal.OP_MENOR) {
				dimensoes = verificaMatriz();
			}

			valor.clear();

			int possivelId = id;

			id++;
			// caso o retorno seja uma chamada de função
			if (tokens.get(id).terminal == Terminal.ABREPARENTESE) {

				verificaChamada(identificador);
				id++;

			}

			id = possivelId;

			// caso o valor retornado tenha sido literal
			while (!(tokens.get(id).terminal == Terminal.OP_IGUAL)) {

				valor.add(tokens.get(id));
				id++;
			}

			Terminal tipoConteudo = verificarTipoVar(valor,
					funcAtual.getVariaveis());
			Variavel varAtual = new Variavel(valor, tipoConteudo, dimensoes);
			if (tipoConteudo == Terminal.IDENTIFICADOR) {
				varAtual = funcAtual.getVariaveis().get(
						tokens.get(possivelId).conteudo);
			}
			// Caso a variavel nao tenha sido declarada nessa função
			if (varAtual == null) {
				// Verifica se a variavel já foi declarada fora da função
				varAtual = variaveis.get(tokens.get(possivelId).conteudo);
				// caso não tenha sido declarada fora
				if (varAtual == null) {
					// verifica se é uma constante
					Constante constAtual = constantes.get(tokens
							.get(possivelId).conteudo);
					// caso não sido declarada como constante
					if (constAtual == null) {
						// verifica se é uma função
						Funcao funcaoAtual = funcoes
								.get(tokens.get(possivelId).conteudo);
						// caso não tenha sido declarada em momento algum
						if (funcaoAtual == null) {
							Erro e = new Erro(tokens.get(possivelId).conteudo,
									1, tokens.get(id).getLinha());
							erros.add(e);
							System.out.println(e.toString());
						}
					}
				}

			}

			if (varAtual != null && varAtual.getDimensoes() > 0
					&& varAtual.getDimensoes() != dimensoes) {
				Erro e = new Erro(tokens.get(possivelId).conteudo, 2, tokens
						.get(id).getLinha());
				erros.add(e);
				System.out.println(e.toString());
			}
			if (varAtual != null && tipoConteudo != funcAtual.getTipo()) {
				if (!(funcAtual.getTipo() == Terminal.REAL && tipoConteudo == Terminal.INTEIRO)) {
					Erro e = new Erro(tokens.get(possivelId).conteudo, 3,
							tokens.get(id).getLinha());
					erros.add(e);
					System.out.println(e.toString());
				}
			}
			// pula o "=", o ">" e o ";"
			id += 3;

			// caso seja o comando leia
		}
		if (tokens.get(id).terminal.equals(Terminal.LEIA)) {
			// pula o "leia" e o "("
			id += 2;
			// Enquanto não chegar ao ")"
			while (!tokens.get(id).terminal.equals(Terminal.FECHAPARENTESE)) {

				if (tokens.get(id).terminal.equals(Terminal.VIRGULA)) {
					// pulando a virgula
					id++;
					continue;
				}

				// caso seja matriz
				if (tokens.get(id).terminal.equals(Terminal.OP_MENOR)) {
					dimensoes = verificaMatriz();
				}

				Variavel varAtual = funcAtual.getVariaveis().get(
						tokens.get(id).conteudo);
				// Caso a variavel nao tenha sido declarada nessa função
				if (varAtual == null) {
					// Verifica se a variavel já foi declarada fora da função
					varAtual = variaveis.get(tokens.get(id).conteudo);
					// caso não tenha sido declarada fora
					if (varAtual == null) {
						// verifica se é uma constante
						Constante constAtual = constantes
								.get(tokens.get(id).conteudo);
						// caso não sido declarada em nenhum momento
						if (constAtual == null) {
							// verifica se é uma função
							Funcao funcaoAtual = funcoes
									.get(tokens.get(id).conteudo);
							// caso não tenha sido declarada em momento algum
							if (funcaoAtual == null) {
								Erro e = new Erro(tokens.get(id).conteudo, 1,
										tokens.get(id).getLinha());
								erros.add(e);
								System.out.println(e.toString());
							}
						}
					}

				}

				if (varAtual != null && varAtual.getDimensoes() > 0
						&& varAtual.getDimensoes() != dimensoes) {
					Erro e = new Erro(tokens.get(id).conteudo, 2, tokens
							.get(id).getLinha());
					erros.add(e);
					System.out.println(e.toString());
				} else if (varAtual != null
						&& constantes.containsKey(tokens.get(id).conteudo)) {
					Erro e = new Erro(tokens.get(id).conteudo, 4, tokens
							.get(id).getLinha());
					erros.add(e);
					System.out.println(e.toString());

				}

				id++;
			}
			// caso seja o comando escreva
		}
		if (tokens.get(id).terminal.equals(Terminal.ESCREVA)) {
			// TODO escreva
			// caso seja o comando se
		}
		if (tokens.get(id).terminal.equals(Terminal.SE)) {
			// TODO se
			// caso seja o comando enquanto
		}
		if (tokens.get(id).terminal.equals(Terminal.ENQUANTO)) {
			// TODO enquanto
		}

	}

	public static void verificaChamada(String identificador) {
		if (funcoes.containsKey(identificador)) {
			Funcao func = funcoes.get(identificador);
			ArrayList<Variavel> parametros = new ArrayList<>();
			// caso haja parametros
			while (!tokens.get(id).terminal.equals(Terminal.FECHAPARENTESE)) {
				id++;

				// caso não haja parametros
				if (tokens.get(id).terminal.equals(Terminal.FECHAPARENTESE)) {
					continue;
				}

				valor.clear();

				int possivelId = id;

				// caso o valor retornado tenha sido literal
				while (!tokens.get(id).terminal.equals(Terminal.FECHAPARENTESE)) {

					valor.add(tokens.get(id));
					id++;
				}

				Terminal tipoConteudo = verificarTipoVar(valor,
						func.getVariaveis());
				Variavel varAtual = new Variavel(valor, tipoConteudo, dimensoes);
				if (tipoConteudo == Terminal.IDENTIFICADOR) {
					varAtual = func.getVariaveis().get(
							tokens.get(possivelId).conteudo);
				}
				// Caso a variavel nao tenha sido declarada nessa função
				if (varAtual == null) {
					// Verifica se a variavel já foi declarada fora da função
					varAtual = variaveis.get(tokens.get(possivelId).conteudo);
					// caso não tenha sido declarada fora
					if (varAtual == null) {
						// verifica se é uma constante
						Constante constAtual = constantes.get(tokens
								.get(possivelId).conteudo);
						// caso não sido declarada como constante
						if (constAtual == null) {

							Erro e = new Erro(tokens.get(possivelId).conteudo,
									1, tokens.get(id).getLinha());
							erros.add(e);
							System.out.println(e.toString());

							while (!tokens.get(id).terminal
									.equals(Terminal.FECHAPARENTESE)) {
								id++;
							}
							return;

						}
					}

				}

				// verifica se é uma função
				Funcao funcaoAtual = funcoes
						.get(tokens.get(possivelId).conteudo);
				if (funcaoAtual != null) {
					String funcao = tokens.get(possivelId).conteudo;
					id++;
					verificaChamada(funcao);
				}

				while (true) {
					// se achar uma virgula ou ")" eh pq terminou o
					// parametro
					if (tokens.get(id).terminal.equals(Terminal.VIRGULA)
							|| tokens.get(id).terminal
									.equals(Terminal.FECHAPARENTESE)) {
						break;
						// enquanto nao achar, tudo eh valor, pois pode ser uma
						// expressao
					} else {
						id++;
					}
				}

				parametros.add(varAtual);

			}
			// checando a qtd de parametros
			if (parametros.size() != func.getParametros().size()) {
				Erro e = new Erro(tokens.get(id).conteudo, 5, tokens.get(id)
						.getLinha());
				erros.add(e);
				System.out.println(e.toString());

				while (!tokens.get(id).terminal.equals(Terminal.FECHAPARENTESE)) {
					id++;
				}
				return;
			}
			// checando o tipo dos parametros
			for (int i = 0; i < parametros.size(); i++) {
				Variavel aux = func.getParametros().get(i);
				Variavel parametro = parametros.get(i);

				// se tiverem dimensões diferentes
				if (aux.getDimensoes() > 0 && parametro.getDimensoes() == 0
						|| aux.getDimensoes() == 0
						&& parametro.getDimensoes() > 0) {
					Erro e = new Erro(tokens.get(id).conteudo, 6, tokens
							.get(id).getLinha());
					erros.add(e);
					System.out.println(e.toString());
				}

				// se tiverem tipos diferentes
				if (!aux.getTipo().equals(parametro.getTipo())) {
					Erro e = new Erro(tokens.get(id).conteudo, 6, tokens
							.get(id).getLinha());
					erros.add(e);
					System.out.println(e.toString());
				}
			}
			// caso a função não tenha sido declarada
		} else {
			Erro e = new Erro(tokens.get(id).conteudo, 7, tokens.get(id)
					.getLinha());
			erros.add(e);
			System.out.println(e.toString());

			while (!tokens.get(id).terminal.equals(Terminal.FECHAPARENTESE)) {
				id++;
			}
		}
	}

	public static int verificaMatriz() {
		throw new UnsupportedOperationException("Not supported yet.");

		// TODO Nao esqueca issaqui nao cara de cu.
	}

	public static void resultado() {

		if (erros.isEmpty()) {
			System.out.println("Sucesso!!!");
		} else {
			for (Erro e : erros) {
				System.out.println(e.toString());
			}
		}
	}
}
