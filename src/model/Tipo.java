package model;

public enum Tipo {

	INTEIRO,
	REAL,
	BOOLEANO,
	VERDADEIRO,
	FALSO,
	CADEIA,
	CARACTERE,
	FUNCAO,
	MATRIZ;
}
