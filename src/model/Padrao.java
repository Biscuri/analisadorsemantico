package model;

import java.util.regex.Pattern;

/*
 * Enum com os padr�es e nomes de tokens poss�veis, ordenado por prioridade.
 * As palavras reservadas s�o um caso especial, por isso n�o h� uma express�o regular para elas.
 * Ao inv�s disso, o lexema � identificado como reservado ou n�o antes de o padr�o ser analisado.
 */

public enum Padrao {
	PROGRAMA("programa"),
	CONST("const"),
	VAR("var"),
	FUNCAO("funcao"),
	INICIO("inicio"),
	FIM("fim"),
	SE("se"),
	ENTAO("entao"), 
	SENAO("senao"),
	ENQUANTO("enquanto"),
	FACA("faca"),
	LEIA("leia"),
	ESCREVA("escreva"),
	INTEIRO("inteiro"),
	REAL("real"),
	BOOLEANO("booleano"),
	VERDADEIRO("verdadeiro"),
	FALSO("falso"),
	CADEIA("cadeia"),
	CARACTERE("caractere"),
	OPLOGICO("nao|e|ou"),
	IDENTIFICADOR("[a-zA-Z]([a-zA-Z]|\\d|_)*"),
	OPARITMETICO("\\+|\\-|\\*|/"),
	NUMERO("\\d+(\\.\\d+)?"),
	OPRELACIONAL("<>|=|<|<=|>|>="),
	PONTOEVIRGULA(";"),
	VIRGULA("\\,"),
	ABREPARENTESE("\\("),
	FECHAPARENTESE("\\)"),
	STRING("\"[a-zA-Z]([a-zA-Z]|\\d|\\p{Blank})*\""),
	CHAR("'([a-zA-Z]|\\d)'");
	public String value;
	
	private Padrao(String value) {
		this.value = value;
	}
        
}
