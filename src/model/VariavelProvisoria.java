package model;

import java.util.ArrayList;
import java.util.List;

public class VariavelProvisoria {

	private ArrayList<Token> valor;
	private Terminal tipo;
	private int dimensoes;
	private String identificador;

	public VariavelProvisoria(ArrayList<Token> valor, Terminal tipo, int dimensoes,
			String identificador) {
		this.valor = valor;
		this.tipo = tipo;
		this.dimensoes = dimensoes;
		this.identificador = identificador;
	}

	public ArrayList<Token> getValor() {
		return valor;
	}

	public void setValor(ArrayList<Token> valor) {
		this.valor = valor;
	}

	public Terminal getTipo() {
		return tipo;
	}

	public void setTipo(Terminal tipo) {
		this.tipo = tipo;
	}

	public int getDimensoes() {
		return dimensoes;
	}

	public void setDimensoes(int dimensoes) {
		this.dimensoes = dimensoes;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

}
