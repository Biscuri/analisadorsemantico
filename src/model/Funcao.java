package model;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class Funcao {

	private ArrayList<Token> valor;
	private Terminal tipo;
	private Hashtable<String, Variavel> variaveis;
        private ArrayList<Variavel> parametros;

	public Funcao(ArrayList<Token> valor, Terminal tipo) {
                parametros = new ArrayList<>();
		variaveis = new Hashtable<>();
		this.valor = valor;
		this.tipo = tipo;
	}

	public Funcao() {
                parametros = new ArrayList<>();
		variaveis = new Hashtable<>();
	}

	public ArrayList<Token> getValor() {
		return valor;
	}

	public void setValor(ArrayList<Token> valor) {
		this.valor = valor;
	}

	public Terminal getTipo() {
		return tipo;
	}

	public void setTipo(Terminal tipo) {
		this.tipo = tipo;
	}

	public Hashtable<String, Variavel> getVariaveis() {
		return variaveis;
	}

	public void setVariaveis(Hashtable<String, Variavel> variaveis) {
		this.variaveis = variaveis;
	}

        public ArrayList<Variavel> getParametros() {
            return parametros;
        }

        public void setParametros(ArrayList<Variavel> parametros) {
            this.parametros = parametros;
        }

}
