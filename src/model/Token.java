package model;
import java.util.regex.*;

import exceptions.TokenNaoConhecidoException;
import static model.Padrao.*;

public class Token {
	
	public String conteudo;
	public Padrao padrao;
	public Terminal terminal;
	public int linha;

    public String getConteudo() {
        return conteudo;
    }

    public Padrao getPadrao() {
        return padrao;
    }

    public int getLinha() {
        return linha;
    }
	
	public Token(String conteudo, int linha) throws TokenNaoConhecidoException{
		this.conteudo = conteudo;
		this.linha = linha;
		Identifica(conteudo);
	}
	
	public Token (String conteudo, Padrao padrao, int linha){
		this.conteudo = conteudo;
		this.padrao = padrao;
		this.linha = linha;
		transformaTerminal();
	}
	
	/*
	 * Auto-identifica o Token, comparando o conte�do com o Regex de todos os padr�es poss�veis.
	 * Se n�o fizer match com nenhum deles, � um s�mbolo inv�lido para o qual ser� gerado um erro.
	 */
	private void Identifica(String conteudo) throws TokenNaoConhecidoException{
		for (Padrao p : Padrao.values()) {
			if (Pattern.matches(p.value, conteudo)){
				padrao = p;
				transformaTerminal();
				return;
			}
		}
		throw new TokenNaoConhecidoException();
	}
	
	private void transformaTerminal(){
		for (Terminal t : Terminal.values()) {
			if (Pattern.matches(t.value, conteudo)){
				terminal = t;
				return;
			}
		}
	}
	
	@Override
	public String toString(){
            String retorno = null;
            if(padrao.equals(PROGRAMA) || padrao.equals(CONST) || padrao.equals(VAR) || padrao.equals(FUNCAO) || padrao.equals(INICIO) || padrao.equals(FIM) || 
                        padrao.equals(SE) || padrao.equals(ENTAO) || padrao.equals(SENAO) || padrao.equals(ENQUANTO) || padrao.equals(FACA) || padrao.equals(LEIA) || 
                        padrao.equals(ESCREVA) || padrao.equals(INTEIRO) || padrao.equals(REAL) || padrao.equals(BOOLEANO) || padrao.equals(VERDADEIRO) || 
                        padrao.equals(FALSO) || padrao.equals(CADEIA) || padrao.equals(CARACTERE)){
                    retorno = "palavra_reservada";
                } if(padrao.equals(IDENTIFICADOR)){
                    retorno = "id";
                } if(padrao.equals(NUMERO)){
                    retorno = "nro";
                } if(padrao.equals(OPARITMETICO) || padrao.equals(OPRELACIONAL) || padrao.equals(OPLOGICO)){
                    retorno = "operador";
                } if(padrao.equals(PONTOEVIRGULA) || padrao.equals(VIRGULA) || padrao.equals(ABREPARENTESE) || padrao.equals(FECHAPARENTESE)){
                    retorno = "delimitador";
                } if(padrao.equals(STRING)){
                    retorno = "cadeia_de_caracteres";
                } if(padrao.equals(CHAR)){
                    retorno = "caractere";
                }
		return linha + " " + conteudo + " " +  retorno;
	}
}
