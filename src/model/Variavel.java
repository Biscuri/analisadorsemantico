package model;

import java.util.ArrayList;
import java.util.List;

public class Variavel {

	private ArrayList<Token> valor;
	private Terminal tipo;
	private int dimensoes;

	public Variavel(ArrayList<Token> valor, Terminal tipo, int dimensoes) {
		this.valor = valor;
		this.tipo = tipo;
		this.dimensoes = dimensoes;
	}

	public ArrayList<Token> getValor() {
		return valor;
	}

	public void setValor(ArrayList<Token> valor) {
		this.valor = valor;
	}

	public Terminal getTipo() {
		return tipo;
	}

	public void setTipo(Terminal tipo) {
		this.tipo = tipo;
	}

	public int getDimensoes() {
		return dimensoes;
	}

	public void setDimensoes(int dimensoes) {
		this.dimensoes = dimensoes;
	}

}
