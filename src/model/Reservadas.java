package model;

public enum Reservadas {
	PROGRAMA("programa"),
	CONST("const"),
	VAR("var"),
	FUNCAO("funcao"),
	INICIO("inicio"),
	FIM("fim"),
	SE("se"),
	ENTAO("entao"), 
	SENAO("senao"),
	ENQUANTO("enquanto"),
	FACA("faca"),
	LEIA("leia"),
	ESCREVA("escreva"),
	INTEIRO("inteiro"),
	REAL("real"),
	BOOLEANO("booleano"),
	VERDADEIRO("verdadeiro"),
	FALSO("falso"),
	CADEIA("cadeia"),
	CARACTERE("caractere");
	
	public String value;
	
	private Reservadas(String value) {
		this.value = value;
	}
}
