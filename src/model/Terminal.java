package model;

public enum Terminal {

	PROGRAMA("programa"),
	CONST("const"),
	VAR("var"),
	FUNCAO("funcao"),
	INICIO("inicio"),
	FIM("fim"),
	SE("se"),
	ENTAO("entao"), 
	SENAO("senao"),
	ENQUANTO("enquanto"),
	FACA("faca"),
	LEIA("leia"),
	ESCREVA("escreva"),
	INTEIRO("inteiro"),
	REAL("real"),
	BOOLEANO("booleano"),
	VERDADEIRO("verdadeiro"),
	FALSO("falso"),
	CADEIA("cadeia"),
	CARACTERE("caractere"),
	OP_E("e"),
	OP_OU("ou"),
	OP_NAO("nao"),
	IDENTIFICADOR("[a-zA-Z]([a-zA-Z]|\\d|_)*"),
	OP_SOMA("\\+"),
	OP_SUB("\\-"),
	OP_MULT("\\*"),
	OP_DIV("/"),
	NUMERO("\\d+(\\.\\d+)?"),
	OP_DIF("<>"),
	OP_IGUAL("="),
	OP_MENOR("<"),
	OP_MENORIGUAL("<="),
	OP_MAIOR(">"),
	OP_MAIORIGUAL(">="),
	OP_RETORNO("=>"),
	PONTOEVIRGULA(";"),
	VIRGULA("\\,"),
	ABREPARENTESE("\\("),
	FECHAPARENTESE("\\)"),
	STRING("\"[a-zA-Z]([a-zA-Z]|\\d|\\p{Blank})*\""),
	CHAR("'([a-zA-Z]|\\d)'"),
	EOS("\\$");
	
	public String value;
	
	private Terminal(String value) {
		this.value = value;
	}
}
