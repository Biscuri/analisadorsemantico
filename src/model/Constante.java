package model;

import java.util.ArrayList;

public class Constante {

	private ArrayList<Token> valor;
	private Terminal tipo;
	private int linha;

	public Constante(ArrayList<Token> valor, Terminal tipo, int linha) {
		this.valor = valor;
		this.tipo = tipo;
		this.setLinha(linha);
	}

	public ArrayList<Token> getValor() {
		return valor;
	}

	public void setValor(ArrayList<Token> valor) {
		this.valor = valor;
	}

	public Terminal getTipo() {
		return tipo;
	}

	public void setTipo(Terminal tipo) {
		this.tipo = tipo;
	}

	public int getLinha() {
		return linha;
	}

	public void setLinha(int linha) {
		this.linha = linha;
	}
	
	@Override
	public String toString(){
		return "Tipo: " + tipo + ". Linha: " + linha + ". Conteudo: " + valor.toString() + "\n";
	}

}
