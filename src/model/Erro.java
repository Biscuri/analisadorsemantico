package model;

import java.util.regex.Pattern;

public class Erro {
	
	private String palavra;

    public String getPalavra() {
        return palavra;
    }

    public String getErro() {
        return erro;
    }

    public int getLinha() {
        return linha;
    }
	private String erro;
	private int linha;
	
	/*
	 * Recebe o lexema incorreto, um c�digo para identificar o erro, e a linha do erro.
	 */

	public Erro(String palavra, int i, int linha) {
		this.palavra = palavra;
		switch(i){
		case 0:
			erro = "Tipos incompativeis.";
			break;
                   
                case 1:
                        erro = "Variavel nao declarada";
                        break;
                       
                case 2: 
                        erro = "Quantidade inválida de dimensões para o array";
                        break;
                        
                case 3:
                        erro = "Retorno invalido";
                        break;
                
                case 4:
                        erro = "Atribuição invalida";
                        break;
                        
                case 5:
                        erro = "Quantidade de parametros invalida";
                        break;
                        
                case 6:
                        erro = "Tipo de parametros invalidos";
                        break;
                        
                case 7:
                        erro = "Funcao não declarada";
		}
		this.linha = linha;
	}
	
	public String toString(){
		return "Erro na linha: " +linha + ". Simbolo: " + palavra + ". Tipo de erro: " + erro + "\n";
	}

}
